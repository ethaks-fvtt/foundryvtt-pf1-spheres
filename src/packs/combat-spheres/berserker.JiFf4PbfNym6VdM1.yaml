_id: JiFf4PbfNym6VdM1
_key: "!journal!JiFf4PbfNym6VdM1"
flags:
  pf1spheres:
    sphere: berserker
folder: null
name: Berserker
pages:
  - _id: 48C3CRQ1wJVWohDH
    _key: "!journal.pages!JiFf4PbfNym6VdM1.48C3CRQ1wJVWohDH"
    flags: {}
    image: {}
    name: Berserker
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/berserker.webp"
        style="float:right;border:none;outline:none" /><p>Berserkers are
        indomitable warriors with boundless reservoirs of endurance and
        unmatched destructive potential. Practitioners of the Berserker sphere
        gain the following abilities:</p>

        <h2 id="toc0"><span style="color:#993300">Berserking</span></h2>

        <p>As a free action at the start of each turn, you may choose to take a
        -2 penalty to AC in exchange for 3 temporary hit points. The penalty and
        the temporary hit points end at the start of your next turn. For every
        point of base attack bonus you have, the granted temporary hit points
        increase by 1.</p>

        <p>See Adrenaline talents, below, for alternative benefits to gaining
        temporary hit points while berserking.</p>

        <p><sup><strong>Note:</strong> Temporary hit points from different
        sources stack. This means you can use Berserking and still benefit from
        other sources of temporary hit points.</sup></p>

        <h2 id="toc1"><span style="color:#993300">Brutal Strike</span></h2>

        <p>As a special attack action, you may make a melee attack against a
        target. Creatures damaged by a brutal strike gain the battered condition
        until the end of your next turn. You may expend martial focus to have
        the attack deal additional damage equal to twice your base attack
        bonus.</p>

        <p>Each brutal strike may be modified by a single (exertion) talent.</p>

        <hr />

        <p><br /><br /><strong>Berserker Talents</strong></p>

        <ul>

        <li>@Compendium[pf1spheres.combat-talents.m5X7CEsME9t6ihpV]{Advancing
        Carnage}</li>

        <li>@Compendium[pf1spheres.combat-talents.Yqq7b5ai1VkM0Qih]{Barbaric
        Throw}</li>

        <li>@Compendium[pf1spheres.combat-talents.DxclnTFehTKkI3IG]{Beat
        Down}</li>

        <li>@Compendium[pf1spheres.combat-talents.g4fDMUqJ0dDdU7o4]{Bloody
        Counter}</li>

        <li>@Compendium[pf1spheres.combat-talents.Rbqr32G4gVhOwcMZ]{Break
        Dancing}</li>

        <li>@Compendium[pf1spheres.combat-talents.rbfP5616p00CSwoc]{Deathless}</li>

        <li>@Compendium[pf1spheres.combat-talents.gPWScX68fNjw6JMH]{Decapitate}</li>

        <li>@Compendium[pf1spheres.combat-talents.gakLMP1LXWLZJmba]{Extended
        Exertion}</li>

        <li>@Compendium[pf1spheres.combat-talents.8IumipmVlcOxf0sL]{Greater
        Sunder}</li>

        <li>@Compendium[pf1spheres.combat-talents.8QwOJWfTPbvXf5NO]{Reaper’s
        Momentum}</li>

        <li>@Compendium[pf1spheres.combat-talents.aAk1EyBb2undGmK2]{Sanguine
        Invigoration}</li>

        <li>@Compendium[pf1spheres.combat-talents.LKl4ygmVrbOrS1TY]{Savage}</li>

        <li>@Compendium[pf1spheres.combat-talents.NbgCSxakHw05Sg0A]{Shatter
        Earth}</li>

        <li>@Compendium[pf1spheres.combat-talents.ifWJ8muVAp4RyNH3]{Sword
        Eater}</li>

        </ul>

        <p><br /><strong>Adrenaline Talents</strong></p>

        <ul>

        <li>@Compendium[pf1spheres.combat-talents.Xcp4y2QYMcXneFsf]{Absolute
        Determination}</li>

        <li>@Compendium[pf1spheres.combat-talents.ffSo71d944B7MAWD]{Dreadnought}</li>

        <li>@Compendium[pf1spheres.combat-talents.18isdDgPlehlTNpD]{Executioner}</li>

        <li>@Compendium[pf1spheres.combat-talents.tiR9AulFblHrpWBJ]{Juggernaut}</li>

        <li>@Compendium[pf1spheres.combat-talents.EzMIzjn6FnB46Cgq]{Marauder}</li>

        <li>@Compendium[pf1spheres.combat-talents.8KYYm7k5Tf6unNTF]{Promethean}</li>

        <li>@Compendium[pf1spheres.combat-talents.lLVohQfaficarqtV]{Specter}</li>

        </ul>

        <p><br /><strong>Exertion Talents</strong></p>

        <ul>

        <li>@Compendium[pf1spheres.combat-talents.HA6kKhnsVDj2sQCK]{Bell-Ringer}</li>

        <li>@Compendium[pf1spheres.combat-talents.kO5r1sxIet5mJmZo]{Bone-Breaker}</li>

        <li>@Compendium[pf1spheres.combat-talents.lqjQDFUIBEXWJfOt]{Flesh
        Carver}</li>

        <li>@Compendium[pf1spheres.combat-talents.xZj6aYZzAQSyakQs]{Heart
        Crusher}</li>

        <li>@Compendium[pf1spheres.combat-talents.pIxJwWX5JzFmxW5l]{Heavy
        Swing}</li>

        <li>@Compendium[pf1spheres.combat-talents.88VSpoOSppsxx1xI]{Leg-Smasher}</li>

        <li>@Compendium[pf1spheres.combat-talents.ZEAWXUwqtjhy2FDC]{Mage
        Masher}</li>

        <li>@Compendium[pf1spheres.combat-talents.JNA6B6Lw5gB4DPvq]{Shieldbreaker}</li>

        <li>@Compendium[pf1spheres.combat-talents.eO6puoqND1K6dF6g]{Shrapnel}</li>

        </ul>

        <p><br /><strong>Legendary Talents</strong></p>

        <ul>

        <li>@Compendium[pf1spheres.combat-talents.tzINtgmzind43vEG]{Alter
        Terrain}</li>

        <li>@Compendium[pf1spheres.combat-talents.t2WFPw9TXLSIciES]{Atavism}</li>

        <li>@Compendium[pf1spheres.combat-talents.jziRSSrbcP9pjKvD]{Flaming
        Ríastrad}</li>

        <li>@Compendium[pf1spheres.combat-talents.I9USjwwCNe3yHSzB]{Genie’s
        Wrath}</li>

        <li>@Compendium[pf1spheres.combat-talents.GkPIfXgslmCCygq5]{Rift Strike,
        Universal}</li>

        <li>@Compendium[pf1spheres.combat-talents.GkPIfXgslmCCygq5]{Rift Strike,
        Universal}</li>

        <li>@Compendium[pf1spheres.combat-talents.HWDD7KHUUwC1GOLd]{Ruinous
        Tread}</li>

        <li>@Compendium[pf1spheres.combat-talents.EysbyoksO5EfJsmT]{Spell
        Sunder}</li>

        </ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
