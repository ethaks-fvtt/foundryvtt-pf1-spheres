_id: fApk9W2BC9KvcstC
_key: "!journal!fApk9W2BC9KvcstC"
flags:
  pf1spheres:
    sphere: trap
folder: null
name: Trap
pages:
  - _id: GNiLIdukALXr4ska
    _key: "!journal.pages!fApk9W2BC9KvcstC.GNiLIdukALXr4ska"
    flags: {}
    image: {}
    name: Trap
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/trap.webp"
        style="float:right;border:none;outline:none" />When you gain the Trap
        sphere, you gain 5 ranks in the Craft (traps) skill, plus 5 ranks per
        additional talent spent in the Trap sphere (maximum ranks equal to your
        total Hit Dice). If you already have ranks in the Craft (traps) skill
        you may immediately retrain them, but you do not get to retrain when
        only temporarily gaining talents, such as through the armiger’s
        customized weapons class feature.

        <p>Possessing this sphere grants you access to the ability to rapidly
        deploy temporary traps. You may make a Craft (traps) check with a DC of
        5 to place a trap on any solid surface within your reach as a full-round
        action. This trap persists for 1 round per point that your check exceed
        the placement DC. The trap is placed in a 5-ft. square of your choice.
        For every 5 ranks in Craft (traps) you possess, you may increase this
        area by one 5-ft. square. All squares a trap occupies must be contiguous
        and you can not place two separate traps in the same squares. Entering a
        trap's space triggers the trap. The Perception DC to locate a trap is 10
        + your Craft (traps) modifier. Trap talents that require a saving throw
        use your ranks in Craft (trap) instead of your base attack bonus when
        determining your saving throw DCs.</p>

        <p>Traps are cobbled together from scrap, detritus in the environment,
        and random bits of string, springs, and other miscellany that have been
        assembled into easily deployed components. Any creature in possession of
        a trap bag is considered to have the required materials to build these
        temporary traps, preparing components during downtime and restocking it
        for negligible cost at regular intervals, though some talents may allow
        the use of more expensive components that must be tracked. At the GM’s
        discretion, some traps may be constructed from materials in the
        environment even without a trap bag.</p>

        <p>You never set off your own traps unless you choose to. Creatures who
        you warn or who see you set a trap are aware of its location and gain a
        +5 circumstance bonus on Perception checks to notice the trap and Reflex
        saves and AC to avoid the trap. A trap may be removed as a full-round
        action with a Disable Device check with a DC equal to your Craft (traps)
        bonus +10. You always succeed on removing your own traps. You may choose
        to recover any expensive components or Alchemy sphere formulae used in a
        successfully removed trap.</p>

        <p>If a trap is perceived, it can be targeted and destroyed by attacks
        without setting it off. A trap has hardness equal to your ranks in Craft
        (traps), AC equal to 10 + your ranks in Craft (traps), and 5 hit points
        + 5 hit points per 2 ranks in Craft (traps) you possess.</p>

        <p>As long as you have martial focus, reduce the time required to set a
        trap from a full-round action to a standard action.</p>

        <p>There are two basic types of temporary traps, snares and darts. A
        trap may only be triggered once unless otherwise noted. Trap sphere
        talents that allow for the use of expensive components allow for the
        component (alchemist fire, dose of poison, etc.) to be drawn as part of
        the action required to place the trap unless noted.</p>

        <h2 id="toc0"><span><span style="color:#993300">Darts</span></span></h2>

        <p>A dart trap is constructed of spring, twine, and usually a needle or
        other blade. A dart targets a line originating from one corner of one of
        the squares it occupies determined when it is set. This line extends out
        to Close range (25 ft. + 5 ft. per 2 ranks of Craft (traps) you possess)
        and stops after one creature is hit. When triggered, make a ranged
        attack roll against each creature in the line until one is hit. You may
        substitute your ranks in Craft (traps) + your Intelligence modifier for
        your base attack bonus + Dexterity modifier on this roll. The dart deals
        1d6 piercing damage, increasing by 1d6 when your Craft (traps) ranks
        reach 3 and every 2 ranks thereafter, and has a critical threat range of
        20 with a x3 multiplier. Some talents allow you to modify your darts.
        Each trap may only benefit from one (dart) talent. Some trap talents
        allow you to forgo dealing damage for benefits as defined in that
        talent.</p>

        <p>You may forgo dart damage to expend an alchemical item (such as a
        tanglefoot bag), a thrown splash weapon (such as alchemist’s fire or
        acid flasks), or a dust (a potion containing a spell that affects an
        area) when making a dart trap to add the effects of that splash weapon
        to the dart. Resolve the attack versus touch AC. Items with an area of
        effect count their origin from the nearest corner of the struck
        creature’s space to the line of the dart’s effect.</p>

        <p>You may apply injury poison to a dart as if it were a weapon after it
        has been set. While setting the trap, you may choose to forgo dart
        damage, instead expending a dose of a contact or inhaled poison as part
        of setting the trap. The dart then resolves its attack vs. touch AC. An
        inhaled poison fills its area as normal, centered on the struck
        creature’s square (or the first square targeted by the line, if the
        creature occupies more than one space).</p>

        <h2 id="toc1"><span><span
        style="color:#993300">Snares</span></span></h2>

        <p>A snare trap is constructed from rope, wire, and similar materials. A
        snare targets the first creature to enter one of the squares it
        occupies. A creature may avoid triggering a snare with a successful
        Reflex save; doing so leaves the trap untriggered and makes the creature
        aware of the trap and able to pass through that trap’s space(s) without
        further risk of triggering it. Some talents grant additional types of
        snares. Each trap may only benefit from one (snare) talent.</p>

        <p>An alchemical item may be rigged to a snare trap. If a creature
        triggers the trap, the alchemical item is activated, targeting a grid
        intersection adjacent to the trap chosen when it is set. If the
        alchemical item is a thrown splash weapon, the triggering creature is
        affected by the item as if it were struck directly by it. Other small
        items such as dyes, dusts, vials of perfume, and other items with GM
        permission may also be attached to a snare trap as well.</p>

        <p>An inhaled poison may be rigged to a snare trap. If a creature
        triggers the trap, the first square entered (the creature’s choice if
        there is more than one entered simultaneously) becomes the centerpoint
        for the inhaled poison.</p>

        <p>When you first gain the Trap sphere, you gain the following
        snare:</p>

        <h4 id="toc2"><span>Tripwire (snare)</span></h4>

        <p>A creature that fails its save against a tripwire snare falls prone.
        Once knocked prone, a creature is no longer at risk of triggering the
        trap in that square. This trap is not destroyed when triggered; instead
        the duration is reduced by 5 rounds per creature that fails its save.
        Running and charging creatures take a -2 penalty on their Reflex saves
        against this trap.</p>

        <hr /><br />

        <br /><b>Trap Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.AGD2WOBm3dz7HNv1]{Aerial
        Trigger}</li>

        <li>@Compendium[pf1spheres.combat-talents.q8zK4iORf8Ak5p1C]{Aimed
        Dart}</li>

        <li>@Compendium[pf1spheres.combat-talents.1cFBxt4G3gX35vYn]{Alchemical
        Trap Master}</li>

        <li>@Compendium[pf1spheres.combat-talents.UazIyamiXuyROnMS]{Combined
        Traps}</li>

        <li>@Compendium[pf1spheres.combat-talents.7nb6HT58cNiuIedb]{Crowd
        Pleaser}</li>

        <li>@Compendium[pf1spheres.combat-talents.q6BuXSkcphBGyN38]{Dirty
        Traps}</li>

        <li>@Compendium[pf1spheres.combat-talents.yD1AbPmvOKrvmFyw]{Expert
        Eye}</li>

        <li>@Compendium[pf1spheres.combat-talents.HKcuBVQzl9Id4W8x]{False
        Trigger}</li>

        <li>@Compendium[pf1spheres.combat-talents.TdsDRFXcvrZH90Uq]{Fool’s
        Retreat}</li>

        <li>@Compendium[pf1spheres.combat-talents.UVmjOUtndrTTR5TY]{Opportunist}</li>

        <li>@Compendium[pf1spheres.combat-talents.MTmsuM9NMFPLmUEK]{Persistent
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.kg9M0XXpKbqvWv77]{Rapid
        Placement}</li>

        <li>@Compendium[pf1spheres.combat-talents.na6J0nh5WoiUc7pi]{Razor
        Wire}</li>

        <li>@Compendium[pf1spheres.combat-talents.lvyh9uGBVpXbvQjX]{Remote
        Trigger}</li>

        <li>@Compendium[pf1spheres.combat-talents.wMVPdG7YwrOdqAgO]{Scatter
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.R4McYDLWxQoZOO2Q]{Sneaky
        Trapper}</li>

        <li>@Compendium[pf1spheres.combat-talents.cJnwP8QULm8eqswH]{Stop Drop
        And Control}</li>

        <li>@Compendium[pf1spheres.combat-talents.ARUFEvKUpSW2qZw4]{Trap
        Door}</li>

        <li>@Compendium[pf1spheres.combat-talents.HAYRa24xEqTbLGm4]{Trap
        Finder}</li>

        <li>@Compendium[pf1spheres.combat-talents.TPvNUZgqgVJ84XRM]{Trap
        Launcher}</li>

        <li>@Compendium[pf1spheres.combat-talents.frILcnhdlXfZDmTU]{Trap
        Wielder}</li>

        <li>@Compendium[pf1spheres.combat-talents.7pl4wSVVvYAlLfQD]{Trapped
        Shield}</li>

        <li>@Compendium[pf1spheres.combat-talents.U1URZgECDs6Kwrvm]{Trapper’s
        Recovery}</li>

        <li>@Compendium[pf1spheres.combat-talents.N0HnD3dYJoUt8Vou]{Trapsmith}</li>

        <li>@Compendium[pf1spheres.combat-talents.mo61ou5EfoCi8Bs5]{Warning}</li>

        </ul><br /><b>Dart Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.Nl5VWhK3US4ZvVt4]{Barbed
        Dart}</li>

        <li>@Compendium[pf1spheres.combat-talents.EI3OfXVBBiaEjKWP]{Blunt
        Dart}</li>

        <li>@Compendium[pf1spheres.combat-talents.vYSo7eGnkfBaPfeh]{Deadly
        Dart}</li>

        <li>@Compendium[pf1spheres.combat-talents.99xquwpcYjX7OOxE]{Flash
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.67PhglZeAwjWtuvq]{Net}</li>

        <li>@Compendium[pf1spheres.combat-talents.XXQ7OuBHbtiAdIFw]{Tethered
        Dart}</li>

        </ul><br /><b>Snare Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.EDvwD0DfFClkSJUp]{Alarm
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.BbpEWibAxS21Fe1N]{Bamboozling
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.izkYar4vHW9BRyC7]{Bear
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.pqa4hrWKpl7U0MSf]{Brutal
        Traps}</li>

        <li>@Compendium[pf1spheres.combat-talents.TiAyVwoqk4v2BVpu]{Disarming
        Traps}</li>

        <li>@Compendium[pf1spheres.combat-talents.99xquwpcYjX7OOxE]{Flash
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.GMoAsGpuNF8LwrVt]{Foam
        Spray}</li>

        <li>@Compendium[pf1spheres.combat-talents.AH82nXsgr63dWaKj]{Magic
        Trigger}</li>

        <li>@Compendium[pf1spheres.combat-talents.67PhglZeAwjWtuvq]{Net}</li>

        <li>@Compendium[pf1spheres.combat-talents.749r7hQxutrpRsz0]{Noose}</li>

        <li>@Compendium[pf1spheres.combat-talents.0FSiYIUHA5g2HkZJ]{Scratching
        Post}</li>

        <li>@Compendium[pf1spheres.combat-talents.ggi96nGowWEaBVMU]{Skunk
        Smoke}</li>

        <li>@Compendium[pf1spheres.combat-talents.wSWy1Wojd6bVlyVQ]{Techno
        Trigger}</li>

        <li>@Compendium[pf1spheres.combat-talents.QGYnDBATrlb94pII]{Terrain
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.LiATGAk9LIDxKYY0]{Tricky
        Traps}</li>

        </ul><br /><b>Legendary Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.0SEtIKcYMsOTXSOM]{A Better
        Mouse Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.Gxf9aX4RVUudkUNt]{All Part Of
        The Plan}</li>

        <li>@Compendium[pf1spheres.combat-talents.zj1lW3YSinTXzQBy]{Ghost-Touch
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.i4BvZm5jIMRzPHh1]{Guillotine}</li>

        <li>@Compendium[pf1spheres.combat-talents.sUFxWi4aPQliO6hb]{Penetrating
        Trap}</li>

        <li>@Compendium[pf1spheres.combat-talents.srEn8xccWBsHJN8k]{Plummeting
        Traps}</li>

        <li>@Compendium[pf1spheres.combat-talents.FUpA2LV1t0MYHfiY]{Temporal
        Snare}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
