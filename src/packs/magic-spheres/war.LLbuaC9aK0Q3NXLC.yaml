_id: LLbuaC9aK0Q3NXLC
_key: "!journal!LLbuaC9aK0Q3NXLC"
flags:
  pf1spheres:
    sphere: war
folder: null
name: War
pages:
  - _id: 4cnb45aZfioaRB2F
    _key: "!journal.pages!LLbuaC9aK0Q3NXLC.4cnb45aZfioaRB2F"
    flags: {}
    image: {}
    name: War
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/war.webp"
        style="float:right;border:none;outline:none" />You can alter a
        battlefield, affecting both allies and enemies with your magic.

        <h2 id="toc0"><span>Totem</span></h2>

        <p>As a standard action, you may place a totem. A totem is an effect on
        a 50-foot + 5 feet per 2 caster level radius area centered on you, but
        does not move as you do (if a totem is created entirely on top of a
        vehicle, it will move with that vehicle but only extend to the edge of
        the vehicle’s space). Creatures who leave this area lose the effects of
        your totem. You must concentrate to maintain a totem, but may always
        spend a spell point as a free action to allow a totem to remain for 1
        minute per caster level without concentration.</p>

        <p>You must remain within long range of the center of your totems when
        maintaining them through concentration. If you move beyond this range,
        the totem ends. The effects of a totem are subject to spell
        resistance.</p>

        <p>When you gain the War sphere, you gain the following totem:</p>

        <h4 id="toc1"><span>Totem Of War</span></h4>

        <p>You and your allies gain a +2 circumstance bonus to all weapon damage
        rolls. This bonus increases by 1 for every 5 caster levels.</p>

        <h2 id="toc2"><span>Rally</span></h2>

        <p>A rally is a magical effect you may enact as an immediate action,
        targeting a creature within the area of effect of one of your totems (or
        with whom you share a mandate, as detailed below), granting the target
        bonuses or allowing them to take certain actions. You must be within
        long range of a target to rally that target. If a rally targets a
        specific creature, it is subject to spell resistance.</p>

        <p>Some rallies can be used in response to other events, in which case
        the rally occurs before the triggering event completes, unless noted
        otherwise. Rallies that work from triggers can only be used when that
        trigger occurs, and can only affect those allies that the trigger
        applies to.</p>

        <p>When you gain the War sphere, you gain the following rally:</p>

        <h4 id="toc3"><span>Commanding Aid</span></h4>

        <p>You may use the aid another action on the target. You must use your
        caster level + your casting ability modifier in place of your melee
        attack bonus for this purpose, and may aid them against any creature
        also within range of your rally.</p>

        <h3 id="toc4"><span>War Talent Types</span></h3>

        <p>Talents marked (totem) grant you new totems. Talents marked (rally)
        grant you additional rallies.</p>

        <h4 id="toc5"><span>Mandate</span></h4>

        <p>Talents marked (mandate) grant you ways of creating mandates.
        Mandates are effects that exist between a pair of allies, each of whom
        benefits from the actions of the other. As a standard action, you can
        create a mandate between two allies within medium range, one of whom may
        be yourself. You may concentrate to maintain a mandate, or can spend a
        spell point to make it last 1 minute per level without concentration. A
        mandate only works while the two sharing the mandate are within medium
        range of each other, and both are conscious and able to act.</p>

        <p>A creature can be part of multiple mandates with the same or
        different creatures, but cannot be a member of the same mandate type
        talent multiple times. Bonuses granted last until the end of the next
        turn of the ally who received the bonus. The advantage of a mandate can
        be triggered over and over again, though it does not stack with
        itself.</p>

        <h4 id="toc6"><span>Momentum</span></h4>

        <p>Talents marked (momentum) grant you a momentum pool, as well as a
        method of using it.</p>

        <p>If you have at least one (momentum) talent, you can spend a spell
        point as a standard action to gain a momentum pool. This pool lasts 1
        hour per caster level, and holds a number of points of momentum equal to
        the War caster’s caster level plus their casting ability modifier. Using
        this ability again refills your momentum pool; it does not create
        additional pools.</p>

        <p>Allies within 30 feet of you can spend points of momentum from this
        pool to activate any ability from any of your (momentum) talents (using
        a (momentum) talent spends momentum points, not spell points). All of
        your (momentum) talents draw from the same pool, and using momentum is
        not considered a sphere ability and does not provoke attacks of
        opportunity.</p>

        <p>The caster level associated with the momentum pools is that of the
        caster when they last filled their pool. Being knocked unconscious or
        made helpless makes the momentum pool inaccessible until you are
        conscious again.</p>

        <hr /><br />

        <br /><b>War Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.xELNdtAXEAvau3Ay]{Bleeding
        Battlefield}</li>

        <li>@Compendium[pf1spheres.magic-talents.FN1Scju3hC6mvyaQ]{Blood
        Bond}</li>

        <li>@Compendium[pf1spheres.magic-talents.XWAeA7GKMZeFJTH9]{Call To
        Arms}</li>

        <li>@Compendium[pf1spheres.magic-talents.lOeVkUZ7jmhq6r1i]{Close
        Cooperation}</li>

        <li>@Compendium[pf1spheres.magic-talents.lJrV1y20yUuPYX2n]{Combat
        Inertia}</li>

        <li>@Compendium[pf1spheres.magic-talents.ybLr9tFkpLirLe8G]{Declaration
        Of War}</li>

        <li>@Compendium[pf1spheres.magic-talents.UcUFZmw7WglfxryN]{Eternal
        Vigilance}</li>

        <li>@Compendium[pf1spheres.magic-talents.OYZq9wHPGTctKqmq]{Hammer And
        Anvil}</li>

        <li>@Compendium[pf1spheres.magic-talents.z7CNX1US1jmibypX]{Lingering
        Resentment}</li>

        <li>@Compendium[pf1spheres.magic-talents.91ykVtpRQlUPkn0v]{Mass
        Rally}</li>

        <li>@Compendium[pf1spheres.magic-talents.W8IoDCTO6m3XIAz8]{Mental
        Assault}</li>

        <li>@Compendium[pf1spheres.magic-talents.FlIvUa0xa6U8aSon]{Ranged
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.0toDUYQXz6COp2Tw]{Redeployment}</li>

        <li>@Compendium[pf1spheres.magic-talents.eDMpCCtDvx7Oyd5G]{Resounding
        Rally}</li>

        <li>@Compendium[pf1spheres.magic-talents.ZyjiNzygezHldQpL]{Resourcefulness}</li>

        <li>@Compendium[pf1spheres.magic-talents.cHiloXzAlpJ8MTm3]{Totem
        Merger}</li>

        <li>@Compendium[pf1spheres.magic-talents.Rbpq4jtD0xC3RwRy]{Totemic
        Aura}</li>

        <li>@Compendium[pf1spheres.magic-talents.7133ckmlOPBOppyo]{Totemic
        Emblem}</li>

        </ul><br /><b>Mandate Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.Jdg6gECrd4qLHyCD]{Adroitness}</li>

        <li>@Compendium[pf1spheres.magic-talents.XcLBF2aoAoOhSKzk]{Aptitude}</li>

        <li>@Compendium[pf1spheres.magic-talents.opS2udK1FJ4oWrUN]{Awareness}</li>

        <li>@Compendium[pf1spheres.magic-talents.CQXEwCQqiHZEu5cF]{Guile}</li>

        <li>@Compendium[pf1spheres.magic-talents.riF8uX7R7pvlp84Q]{Impose}</li>

        <li>@Compendium[pf1spheres.magic-talents.HASaNq9Mga6Febik]{Mobility}</li>

        <li>@Compendium[pf1spheres.magic-talents.wqXPNbDNNMOq8W1a]{Perseverance}</li>

        <li>@Compendium[pf1spheres.magic-talents.UaNdfNG6RKYaRMty]{Resolve}</li>

        <li>@Compendium[pf1spheres.magic-talents.rzDsKd9Y8U3ZBbri]{Ruthlessness}</li>

        <li>@Compendium[pf1spheres.magic-talents.zeepezYv904m91Wx]{Tenacity}</li>

        <li>@Compendium[pf1spheres.magic-talents.JJJ3FPom41GgCYFw]{Vindictiveness}</li>

        </ul><br /><b>Momentum Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.IE56S5KQtvQHmzxO]{Aggressive
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.9WItFYFYkPGMhx4i]{Cooperative
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.RMS8rBJNpgFTnSSI]{Damaging
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.Am27AwPvVbJkmfvx]{Demoralizing
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.MEoihwSfGcM4TsdC]{Disrupting
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.VxOp87w5OxwDp7YJ]{Favorable
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.lqxyDrxGcyv2vntB]{Focusing
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.gohW3WibZT7RY3aT]{Marauding
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.TpUP0CcMAWUQicSY]{Resilient
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.zI5GEc2EOh6ZWMOO]{Revitalizing
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.C2WPyy0YJyBV9Fud]{Tactical
        Momentum}</li>

        <li>@Compendium[pf1spheres.magic-talents.xO4Lx3ngArTq4uHD]{Threatening
        Momentum}</li>

        </ul><br /><b>Rally Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.VsImi2U7TEfp83jn]{Absorb}</li>

        <li>@Compendium[pf1spheres.magic-talents.BBmnlSEU7q4nyDe7]{Counterattack}</li>

        <li>@Compendium[pf1spheres.magic-talents.p8jIW4ZEm1ZBwD5J]{Empower}</li>

        <li>@Compendium[pf1spheres.magic-talents.VT3gPUO5Ro0fUIG7]{Engage}</li>

        <li>@Compendium[pf1spheres.magic-talents.uBqDgERycHayGUZb]{Finish}</li>

        <li>@Compendium[pf1spheres.magic-talents.GXwwctwK2Fj80uZq]{Intercept}</li>

        <li>@Compendium[pf1spheres.magic-talents.qve9rNdlIv5zRE2m]{Position}</li>

        <li>@Compendium[pf1spheres.magic-talents.rkpw7y0V5POIGRr7]{Replenish}</li>

        <li>@Compendium[pf1spheres.magic-talents.83Hno2o2u8LmKviY]{Retribution}</li>

        <li>@Compendium[pf1spheres.magic-talents.0SoYHlkW1PcqKg6Q]{Safety}</li>

        <li>@Compendium[pf1spheres.magic-talents.WiJx22a6V3yk7fhw]{Strike}</li>

        </ul><br /><b>Totem Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.7ouSd7ML4GA0oNzs]{Absolute
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.aUXr5HKG9fdfFtKO]{Blood
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.83S0HZK8lZkSRC1q]{Divisive
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.X0lxPhS673FCdYzJ]{Giving
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.DHJoOkmBRd5PoOnT]{Gyroscopic
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.LJSsCCzOXEJvs11a]{Hallowed/Unhallowed
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.gUK0yJ2DpXbE9SSU]{Haunting
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.7I0sv9BSifbkjK87]{Invigorating
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.wzJvMe21G1zuVFk9]{Quickening
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.vgwrQpkynicGFKXl]{Rebellious
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.QJW3cPYNvCPH5uRi]{Scourging
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.O046nIPTFgH3BGI5]{Tactical
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.Pupj1AUAH5DsCwFR]{Taking
        Totem}</li>

        <li>@Compendium[pf1spheres.magic-talents.tah6ZYcEJxanNuO9]{Totem Of
        Agility}</li>

        <li>@Compendium[pf1spheres.magic-talents.zuR1DARrW3v24AMS]{Totem Of
        Allegiance}</li>

        <li>@Compendium[pf1spheres.magic-talents.fqVUlyrO4I3h4Fub]{Totem Of
        Courage}</li>

        <li>@Compendium[pf1spheres.magic-talents.I0jYNa0kU8tRCv0W]{Totem Of Deep
        Thought}</li>

        <li>@Compendium[pf1spheres.magic-talents.BUtN93njV5oe0WEx]{Totem Of
        Doom}</li>

        <li>@Compendium[pf1spheres.magic-talents.AaDegaD18ZVURQhN]{Totem Of
        Dread}</li>

        <li>@Compendium[pf1spheres.magic-talents.NiwCFkDS7bRFbN8N]{Totem Of
        Enemies}</li>

        <li>@Compendium[pf1spheres.magic-talents.G3hV0kSRR0ut9euE]{Totem Of
        Expulsion}</li>

        <li>@Compendium[pf1spheres.magic-talents.rhfJxzLdBUpYH0te]{Totem Of
        Foresight}</li>

        <li>@Compendium[pf1spheres.magic-talents.xhcoKTcl183K0P0y]{Totem Of
        Insanity}</li>

        <li>@Compendium[pf1spheres.magic-talents.PimA6XBRwYJXWuTL]{Totem Of
        Iron}</li>

        <li>@Compendium[pf1spheres.magic-talents.CYMo3ozA7WJXoCzL]{Totem Of
        Liberation}</li>

        <li>@Compendium[pf1spheres.magic-talents.nd0Zs8sf9dzizx3j]{Totem Of
        Mobility}</li>

        <li>@Compendium[pf1spheres.magic-talents.gDgsM3NHM5I2LNp1]{Totem Of
        Screaming Skin}</li>

        <li>@Compendium[pf1spheres.magic-talents.I5Kbrb559UeBIImA]{Totem Of
        Shared Sight}</li>

        <li>@Compendium[pf1spheres.magic-talents.SUSlkwylXdErku6m]{Totem Of
        Speed}</li>

        <li>@Compendium[pf1spheres.magic-talents.tvYdxOECVnt37ErL]{Totem Of
        Stability}</li>

        <li>@Compendium[pf1spheres.magic-talents.WBvX0Z7JitJBgIOH]{Totem Of
        Stumbling}</li>

        <li>@Compendium[pf1spheres.magic-talents.GuZjbfyW6lcq3o6X]{Totem Of
        Tactical Coordination}</li>

        <li>@Compendium[pf1spheres.magic-talents.1DOqlrwY5WKZbrhZ]{Totem Of
        Tactical Prowess}</li>

        <li>@Compendium[pf1spheres.magic-talents.gnkzWnNQ4VMXM3ub]{Totem Of The
        Dragonslayer}</li>

        <li>@Compendium[pf1spheres.magic-talents.4Gii57VGmy7MJCAY]{Totem Of The
        Heroic Heart}</li>

        <li>@Compendium[pf1spheres.magic-talents.fdS8rSGl6gwEUFsY]{Totem Of The
        War Dance}</li>

        <li>@Compendium[pf1spheres.magic-talents.CTLYOyLXVE8E5x2r]{Totem Of
        Whispers}</li>

        <li>@Compendium[pf1spheres.magic-talents.JbslaoCCadlICIHR]{What Is It
        Good For?}</li>

        </ul><br /><b>Advanced War Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.w4RL5wPpEtIM9Xh0]{Carry The
        Flag}</li>

        <li>@Compendium[pf1spheres.magic-talents.CnL2RrV1TZ7tXoKE]{Commander}</li>

        <li>@Compendium[pf1spheres.magic-talents.ktDh5AgbIuraflaY]{Monument}</li>

        <li>@Compendium[pf1spheres.magic-talents.KFSP2xRXHvrORnsp]{Penetrating
        Totems}</li>

        <li>@Compendium[pf1spheres.magic-talents.Z3G62LTjxvtA6Fui]{Totem Of
        Tenuous Mortality}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
