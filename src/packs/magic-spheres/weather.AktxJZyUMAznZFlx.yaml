_id: AktxJZyUMAznZFlx
_key: "!journal!AktxJZyUMAznZFlx"
flags:
  pf1spheres:
    sphere: weather
folder: null
name: Weather
pages:
  - _id: kIcg8FhTIxYeNcjI
    _key: "!journal.pages!AktxJZyUMAznZFlx.kIcg8FhTIxYeNcjI"
    flags: {}
    image: {}
    name: Weather
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/weather.webp"
        style="float:right;border:none;outline:none" />You can command the
        weather to do your bidding.

        <h2 id="toc0"><span>Control Weather</span></h2>

        <p>As a standard action, you may control all weather within medium
        range, adjusting either the wind, temperature, or precipitation levels.
        If you are in a confined area such as inside a building, your control
        only extends to the edge of that space.</p>

        <p>This change in weather lasts as long as you concentrate, but you may
        always spend a spell point as a free action to allow this change to
        continue for 1 minute per caster level without concentration. When using
        control weather to change the weather’s severity, the change happens 1
        level change per category per round until the desired severity is
        reached. When the effect ends, the severity of the altered weather
        categories returns to normal by 1 step per round.</p>

        <p>If you are maintaining the effect through concentration, the effect
        moves with you, raising and lowering the severity of unaffected or
        no-longer affected area as described above. If it is being maintained
        through a spell point, it remains stationary.</p>

        <p>Wind has 7 steps of severity, while temperature and humidity have 13.
        Temperature is divided between ‘Heat’ and ‘Cold’, each with 7 steps of
        severity. (If the temperature is lowered below step 1 of Cold, it
        becomes step 2 of Heat. If the temperature is lowered below step 1 of
        Heat, it becomes step 2 of Cold.) Likewise, humidity is divided between
        ‘Precipitation’ and ‘Aridity’, which follows a similar setup. An average
        day of no wind, no rain, and unremarkable temperature is assumed to be
        at severity level 1 for all categories.</p>

        <p>At 1st caster level, you may create weather of severity level 1, 2 or
        3. This improves by one severity level per 7 caster levels to a maximum
        of severity level 7. You may create weather of up to this severity, or
        lower the severity of pre-existing weather if it is within this limit.
        If the natural weather is of a higher severity level than you can
        affect, you cannot use control weather to alter that aspect of the
        weather.</p>

        <p>If two casters are controlling weather in the same location and
        affecting different categories, both effects happen normally.</p>

        <p>If both casters are affecting the same category, the second caster
        must be able to affect the weather’s new severity, and must succeed at a
        magic skill check to wrestle control from the first caster. On the first
        caster’s subsequent turn, if he maintains his own control weather
        effect, he must succeed at his own magic skill check to wrestle control
        of the weather back from the second caster. If the first caster is
        maintaining their effect through a spell point instead of concentration
        and the second caster succeeds at their magic skill check, the first
        caster’s weather effect is suppressed for as long as the second caster
        uses control weather. Once the second caster’s effect ends, the first
        caster’s effect resumes functioning, provided its duration has not
        already expired.</p>

        <p>The following is a description of all the different effects one can
        create with the Weather sphere. See the Pathfinder Core rulebook for
        more details on weather and environmental effects. Depending on the
        terrain, a GM could rule additional effects happen; rain can cause
        rivers or enclosed spaces to flood, Cold can create ice sheets on flat
        terrain, etc. Generally, weather conditions from different categories
        stack. (Thus, if Wind, Cold, and Precipitation were all increased to
        Severity level 5, the area would be under the effects of the appropriate
        Wind, Cold, and Snow effects, all at the same time.)</p>

        <p><strong>Note:</strong> It is impossible to raise weather above
        severity level 7. However, for every effective level above severity
        level 7 a caster’s control weather would otherwise be able to reach, all
        numbers associated with that weather type, including DCs or penalties,
        are increased by 1.</p>

        <strong>Table: Weather Conditions</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Severity</th>

        <th>Wind</th>

        <th>Cold</th>

        <th>Heat</th>

        <th>Precipitation</th>

        <th>Aridity</th>

        <th>Ash</th>

        <th>Vog</th>

        <th>Fallout</th>

        </tr>

        <tr>

        <td>1</td>

        <td>Light</td>

        <td>Cool</td>

        <td>Cool</td>

        <td>None</td>

        <td>None</td>

        <td>None</td>

        <td>None</td>

        <td>None</td>

        </tr>

        <tr>

        <td>2</td>

        <td>Moderate</td>

        <td>Chilled</td>

        <td>Warm</td>

        <td>Mist</td>

        <td>Dry</td>

        <td>Light</td>

        <td>Light</td>

        <td>Light</td>

        </tr>

        <tr>

        <td>3</td>

        <td>Strong</td>

        <td>Cold</td>

        <td>Hot</td>

        <td>Light/fog</td>

        <td>Very dry</td>

        <td>Moderate</td>

        <td>Moderate</td>

        <td>Moderate</td>

        </tr>

        <tr>

        <td>4</td>

        <td>Severe</td>

        <td>Severe</td>

        <td>Severe</td>

        <td>Moderate</td>

        <td>Parched</td>

        <td>Heavy</td>

        <td>Heavy</td>

        <td>Heavy</td>

        </tr>

        <tr>

        <td>5</td>

        <td>Windstorm</td>

        <td>Extreme</td>

        <td>Extreme</td>

        <td>Heavy</td>

        <td>Drought</td>

        <td>Very Heavy</td>

        <td>Very Heavy</td>

        <td>Very Heavy</td>

        </tr>

        <tr>

        <td>6</td>

        <td>Hurricane</td>

        <td>Arctic</td>

        <td>Burning</td>

        <td>Flash flood</td>

        <td>Sere</td>

        <td>Suffocating</td>

        <td>Suffocating</td>

        <td>Withering</td>

        </tr>

        <tr>

        <td>7</td>

        <td>Tornado</td>

        <td>Killing</td>

        <td>Boiling</td>

        <td>Great flood</td>

        <td>Desiccating</td>

        <td>Smothering</td>

        <td>Smothering</td>

        <td>Irradiating</td>

        </tr>

        </tbody></table>

        <h4 id="toc1"><span>Wind</span></h4>

        <p>You cannot change the direction of the wind, but you may overpower
        it. If you wish to change the direction of the wind, you must create a
        new wind of the direction you desire. If the wind is the same severity
        as the natural wind, the winds negate (if they oppose) or join to create
        a wind with a direction halfway between the two. If one wind is smaller
        than the other, the smaller wind is negated in favor of the stronger
        one.</p>

        <p><strong>Light Wind:</strong> A gentle breeze, having little or no
        game effect.</p>

        <p><strong>Moderate Wind:</strong> A steady wind with a 50% chance of
        extinguishing small, unprotected flames, such as candles.</p>

        <p><strong>Strong Wind:</strong> Gusts that automatically extinguish
        unprotected flames (candles, torches, and the like). Such gusts impose a
        –2 penalty on ranged attack rolls and on Perception checks.</p>

        <p><strong>Severe Wind:</strong> In addition to automatically
        extinguishing any unprotected flames, winds of this magnitude cause
        protected flames (such as those of lanterns) to dance wildly and have a
        50% chance of extinguishing these lights. Ranged weapon attacks and
        Perception checks are at a –4 penalty.</p>

        <p><strong>Windstorm:</strong> Powerful enough to bring down branches if
        not whole trees, windstorms automatically extinguish unprotected flames
        and have a 75% chance of blowing out protected flames, such as those of
        lanterns. Ranged weapon attacks are impossible, and even siege weapons
        have a –4 penalty on attack rolls. Perception checks that rely on sound
        are at a –8 penalty due to the howling of the wind.</p>

        <p><strong>Hurricane-Force Wind:</strong> All flames are extinguished.
        Ranged attacks are impossible (except with siege weapons, which have a
        –8 penalty on attack rolls). Perception checks based on sound are
        impossible: all characters can hear is the roaring of the wind.
        Hurricane-force winds often fell trees.</p>

        <p><strong>Tornado:</strong> All flames are extinguished. All ranged
        attacks are impossible (even with siege weapons), as are sound-based
        Perception checks. While natural winds of severity level 7 can result in
        a tornado, magically-altered winds of severity level 7 affects too small
        of an area to create this phenomenon. (Instead of being blown away [see
        Table: Wind], characters in close proximity to a tornado who fail their
        Fortitude saves are sucked toward the tornado. Those who come in contact
        with the actual funnel cloud are picked up and whirled around for 1d10
        rounds, taking 6d6 points of damage per round, before being violently
        expelled (falling damage might apply). While a tornado’s rotational
        speed can be as great as 300 mph, the funnel itself moves forward at an
        average of 30 mph [roughly 250 feet per round]. A tornado uproots trees,
        destroys buildings, and causes similar forms of major destruction.)</p>

        <strong>Table: Wind</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Severity Level</th>

        <th>Wind Speed</th>

        <th>Ranged Attacks Normal/Siege Weapons^</th>

        <th>Checked Size^^</th>

        <th>Blown Away Size^^^</th>

        <th>Fly Penalty</th>

        </tr>

        <tr>

        <td>1 (light)</td>

        <td>0–10 mph</td>

        <td>—/—</td>

        <td>—</td>

        <td>—</td>

        <td>—</td>

        </tr>

        <tr>

        <td>2 (moderate)</td>

        <td>11–20 mph</td>

        <td>—/—</td>

        <td>—</td>

        <td>—</td>

        <td>—</td>

        </tr>

        <tr>

        <td>3 (strong)</td>

        <td>21–30 mph</td>

        <td>–2/—</td>

        <td>Tiny</td>

        <td>—</td>

        <td>–2</td>

        </tr>

        <tr>

        <td>4 (severe)</td>

        <td>31–50 mph</td>

        <td>–4/—</td>

        <td>Small</td>

        <td>Tiny</td>

        <td>–4</td>

        </tr>

        <tr>

        <td>5 (windstorm)</td>

        <td>51–74 mph</td>

        <td>Impossible/–4</td>

        <td>Medium</td>

        <td>Small</td>

        <td>–8</td>

        </tr>

        <tr>

        <td>6 (hurricane)</td>

        <td>75–174 mph</td>

        <td>Impossible/–8</td>

        <td>Large</td>

        <td>Medium</td>

        <td>–12</td>

        </tr>

        <tr>

        <td>7 (tornado)</td>

        <td>175–300 mph</td>

        <td>Impossible/impossible</td>

        <td>Huge</td>

        <td>Large</td>

        <td>–16</td>

        </tr>

        </tbody></table>

        <p>^ The siege weapon category includes ballista and catapult attacks as
        well as boulders tossed by giants.<br />

        ^^ <strong>Checked Size:</strong> Creatures of this size or smaller are
        unable to move forward against the force of the wind unless they succeed
        on a DC 10 Strength check (if on the ground) or a DC 20 Fly check if
        airborne.<br />

        ^^^ <strong>Blown Away Size:</strong> Creatures on the ground are
        knocked prone and rolled 1d4 × 10 feet, taking 1d4 points of nonlethal
        damage per 10 feet, unless they succeed at a DC 15 Strength check.
        Flying creatures are blown back 2d6 × 10 feet and take 2d6 points of
        nonlethal damage due to battering and buffeting, unless they succeed at
        a DC 25 Fly check.</p>

        <p><strong>Other Wind Effects</strong><br />

        <strong>Duststorm:</strong> When severity level 4 winds are created in a
        desert, it can create a duststorm, blowing fine grains of sand that
        obscure vision, smother unprotected flames, and can even choke protected
        flames (50% chance). At severity level 5, a duststorm deals 1d3 points
        of nonlethal damage each round to anyone caught out in the open without
        shelter and also poses a choking hazard (see Drowning, except that a
        character with a scarf or similar protection across her mouth and nose
        does not begin to choke until after a number of rounds equal to 10 + her
        Constitution score).</p>

        <h4 id="toc2"><span>Cold</span></h4>

        <p>Cold environments can deal either lethal or nonlethal cold damage to
        a creature. A creature dealt damage in this manner becomes fatigued
        (frostbitten), and cannot recover from fatigue or damage until warmed
        up. If a character takes an amount of nonlethal cold damage equal to her
        total hit points, any further damage from a cold environment is lethal
        cold damage.</p>

        <p>Characters wearing a cold weather suit treat the Cold as if it were 1
        level lower in severity, and may use the Survival skill to gain bonuses
        to saving throws against Cold. A large fire can be used to create an
        area of warmth in a cold environment. Cold deals damage according to
        Table: Cold.</p>

        <strong>Table: Cold</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Severity Level</th>

        <th>Effects</th>

        </tr>

        <tr>

        <td>3 (below 40° F)</td>

        <td>Fortitude save each hour (DC 15, +1 per previous check) or take 1d6
        points of nonlethal cold damage.</td>

        </tr>

        <tr>

        <td>4 (below 0° F)</td>

        <td>Same as level 3, but a check every 10 minutes.</td>

        </tr>

        <tr>

        <td>5 (below –20° F)</td>

        <td>1d6 lethal cold damage every minute (no save) and a Fortitude save
        (DC 15, +1 per previous check) or take 1d4 nonlethal cold damage.</td>

        </tr>

        <tr>

        <td>6 (below -60° F)</td>

        <td>Same as severity level 5, but damage and Fortitude saves happen each
        round.</td>

        </tr>

        <tr>

        <td>7 (below -120° F)</td>

        <td>3d6 lethal cold damage each round (no save). Being encased in ice
        increases this to 10d6.</td>

        </tr>

        </tbody></table>

        <h4 id="toc3"><span>Heat</span></h4>

        <p>Heat works very similarly to Cold. Hot environments can deal lethal
        or nonlethal fire damage to a creature. A creature dealt damage in this
        manner becomes fatigued (heatstroke), and cannot recover from fatigue or
        damage until cooled off (reaches shade, survives until nightfall, gets
        doused in water, and so forth). If a character takes an amount of
        nonlethal fire damage equal to her total hit points, any further damage
        from a hot environment is lethal fire damage.</p>

        <p>Characters in heavy clothing or armor take a -4 penalty on their
        saves against Heat. Creatures may use the Survival skill to gain bonuses
        to saving throws against Heat. Heat deals damage according to Table:
        Heat.</p>

        <strong>Table: Heat</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Severity Level</th>

        <th>Effects</th>

        </tr>

        <tr>

        <td>3 (above 90° F)</td>

        <td>Fortitude save each hour (DC 15, +1 per previous check) or take 1d4
        points of nonlethal fire damage.</td>

        </tr>

        <tr>

        <td>4 (above 110° F)</td>

        <td>Same as level 3, but a check every 10 minutes.</td>

        </tr>

        <tr>

        <td>5 (above 140° F)</td>

        <td>1d6 lethal fire damage every minute (no save), and a Fortitude save
        every 5 minutes (DC 15, +1 per previous check) or take 1d4 nonlethal
        fire damage.</td>

        </tr>

        <tr>

        <td>6 (above 180° F)</td>

        <td>Same as severity level 5, but damage and Fortitude saves happen each
        round.</td>

        </tr>

        <tr>

        <td>7 (above 212° F)</td>

        <td>3d6 lethal fire damage each round, no save. Immersion in boiling
        liquids increases this to 10d6.</td>

        </tr>

        </tbody></table>

        <h4 id="toc4"><span>Precipitation</span></h4>

        <p>Precipitation has the most severe interaction with the other weather
        categories, as the conditions change depending on the temperature and
        wind.</p>

        <p>When combined with Cold severity 4 or higher, snow effects are added
        to rain effects. When combined with Wind severity 4 or higher, storm
        effects are added to the rain effects. When combined with both Cold
        severity 4 and Wind severity 4, this results in rain, snow, and storm
        effects. Mist and fog are the exceptions to this, as they only appear
        when rain is not combined with either snow nor storm.</p>

        <p>Severity of the snow or storm effects depends on the severity of the
        Precipitation, not the severity of the Wind or Cold.</p>

        <p><strong>Note:</strong> While water freezes at Cold severity 3, magic
        cannot cause rain to instantly become snow as it falls until severity
        level 4, since the magical water/cold is only augmenting the natural
        process, not replacing it.</p>

        <strong>Table: Precipitation Severity</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Level</th>

        <th>Rain Effects</th>

        <th>Combined with Cold 4 and higher (Snow Effects)</th>

        <th>Combined with Winds 4 and higher (Storm Effects)</th>

        </tr>

        <tr>

        <td>1</td>

        <td>None</td>

        <td>None</td>

        <td>None</td>

        </tr>

        <tr>

        <td>2</td>

        <td>Mist</td>

        <td>Light frost</td>

        <td>Mist</td>

        </tr>

        <tr>

        <td>3</td>

        <td>Light/fog</td>

        <td>Snow</td>

        <td>Light storm</td>

        </tr>

        <tr>

        <td>4</td>

        <td>Moderate</td>

        <td>Heavy snow</td>

        <td>Storm</td>

        </tr>

        <tr>

        <td>5</td>

        <td>Heavy</td>

        <td>Blizzard</td>

        <td>Powerful storm</td>

        </tr>

        <tr>

        <td>6</td>

        <td>Flash flood</td>

        <td>Great blizzard</td>

        <td>Monsoon</td>

        </tr>

        <tr>

        <td>7</td>

        <td>Great flood</td>

        <td>Avalanche</td>

        <td>Typhoon</td>

        </tr>

        </tbody></table>

        <p><strong>Rain Effects</strong><br />

        <strong>Mist:</strong> Mist grants all creatures concealment from any
        creatures over 100 feet away (all attacks suffer a 20% miss chance).</p>

        <p><strong>Fog:</strong> The caster may create light rain or fog. If fog
        is chosen, it obscures all sight beyond 5 feet, including darkvision.
        Creatures 5 feet away have concealment.</p>

        <p><strong>Other rain effects:</strong> Beginning at severity level 4,
        rain has the same effect on fires, ranged attacks, and Perception checks
        as wind of equal severity level. This does not stack with the penalties
        provided by wind. The rain also cuts visibility ranges by half,
        resulting in an additional –4 penalty on Perception checks due to poor
        visibility. It rains about 1 inch per hour at severity 4, 2 inches per
        hour at severity level 5, 5 inches per hour at severity level 6, and 10
        inches per hour at severity level 7.</p>

        <p><strong>Snow Effects</strong><br />

        Snow causes squares to count as difficult terrain. This requires 24
        hours of snow at severity level 1, 8 hours at severity level 2, 1 hour
        at severity level 3, and happens immediately at severity level 4. At
        severity level 5, snow obscures vision as fog does. It costs 4 squares
        of movement to enter a square covered with heavy snow (about 2 feet).
        This requires 24 hours at severity level 4, 8 hours at severity level 5,
        1 hour at severity level 6, and happens immediately at severity level 7.
        Heavy snow accompanied by strong or severe winds might also result in
        snowdrifts 1d4 × 5 feet deep, especially in and around objects big
        enough to deflect the wind—a cabin or a large tent, for instance.</p>

        <p><strong>Storm Effects</strong><br />

        Beginning at severity level 4, storms will randomly strike a square with
        lightning, dealing 4d8 electricity damage (Reflex half) to everything in
        or above that square. This happens once per minute. This damage
        increases by 2d8 for every severity level above 4, to a maximum of 10d8
        at severity level 7.</p>

        <h4 id="toc5"><span>Aridity</span></h4>

        <p>As Heat is the opposite of Cold, Aridity is the opposite of
        Precipitation. If the Precipitation is lowered below step 1 of
        Precipitation, it becomes step 2 of Aridity. If the Aridity is lowered
        below step 1 of Aridity, it becomes step 2 of Precipitation. Water left
        in open air loses about an inch of height per day for every level of
        combined severity between Heat and Aridity</p>

        <p>This becomes 2 inches per combined level at Aridity severity level 6,
        and 4 inches per combined level for Aridity severity level 7.</p>

        <p>In normal conditions, a character can go without water for 1 day plus
        a number of hours equal to his Constitution score. After this time, the
        character must succeed at a Constitution check each hour (DC 10, +1 for
        each previous check) or take 1d6 points of nonlethal damage. Characters
        that take an amount of nonlethal damage equal to their total hit points
        begin to take lethal damage instead.</p>

        <p>Beginning at severity level 2, all creatures suffer a penalty equal
        to the Aridity severity level to all saving throws against effects that
        would cause fatigue or exhaustion, as well as to saving throws made
        against thirst. At Aridity severity level 3 creatures must drink twice
        as much water per day to stay healthy.</p>

        <p>At severity level 4, the amount of time a character can go without
        water before they must begin attempting checks, as well as the time
        between checks, is cut in half (12 + 1/2 Constitution modifier hours,
        checks every 30 minutes). This time is cut in half again for each
        severity level above 4.</p>

        <p>Even if they are drinking enough water, targets in areas of extreme
        magically-enhanced aridity still suffer terrible effects as the water in
        their bodies is constantly sucked up by the environment around them.
        Targets in areas of high aridity suffer the same chance of suffering
        nonlethal damage (from desiccation) and suffering fatigue (but not fire
        damage) as if they were in Heat of the same severity level. This does
        not stack with the penalties provided by Heat or Cold. However, if
        combined with Wind or Heat of severity level 4 or higher, the combined
        effects drain the energy from all living creatures, rendering them
        sickened for as long as they are within this environment.</p>

        <p>Characters who have taken nonlethal damage due to desiccation or from
        a lack of food or water are fatigued. Nonlethal damage from thirst or
        starvation cannot be recovered until the character gets food or water,
        as needed, or until they receive magical healing.</p>

        <h3 id="toc6"><span>Other Weather Effects</span></h3>

        <p>There are weather categories that are not able to be manipulated by
        basic wielders of control weather, but can become available through
        taking certain talents. These weather categories are as follows:</p>

        <h4 id="toc7"><span>Ash</span></h4>

        <p>Ash is treated as snow, except that, beginning at severity level 4,
        any creature inhaling it must succeed at a DC 15 Fortitude save each
        round or be staggered for one round. The DC increases by 1 for each
        previous save and by 2 for each severity category above 4. Any creature
        moving through difficult terrain created by ash takes 1d6 slashing
        damage for every 10 feet they move (rounded up). Moving through heavy
        ash causes 1d6 slashing damage for every 5 feet.</p>

        <p>Ash is a type of volcanic weather, and thus requires the Volcano Lord
        advanced talent to create.</p>

        <h4 id="toc8"><span>Vog</span></h4>

        <p>Vog of severity level 2 and 3 act as mist and fog. Starting at
        severity level 4 it also causes all in the area to become sickened until
        it leaves the area (Fortitude DC 15 negates, the DC increases by 1 per
        previous save). For each severity level above 4, the DC increases by 2.
        At severity level 6, all within the area are nauseated as long as they
        remain in the area and for 1d6+1 rounds after they exit. At severity
        level 7, they also suffer 1d6 Constitution damage per round as they
        inhale poisonous gas (Fortitude DC 15 negates, the DC increases by 1 per
        previous save).</p>

        <p>Vog is a type of volcanic weather, and thus requires the Volcano Lord
        advanced talent to create.</p>

        <h4 id="toc9"><span>Fallout [Cata. HB]</span></h4>

        <p>Fallout of severity level 3 forces creatures inside the area to
        attempt a Fortitude save each hour (DC 15, +1 per previous check) or
        take 1 point of Constitution damage.</p>

        <p>Severity level 4 functions as severity level 3, save that checks must
        be made every 10 minutes rather than every hour.</p>

        <p>Severity level 5 functions as severity level 4, save that checks must
        be made every minute rather than every hour and Constitution damage
        improves to 1d2.</p>

        <p>Severity level 6 functions as severity level 5, save that checks must
        be made every round rather than every minute and the Constitution damage
        becomes 1d4 Constitution drain.</p>

        <p>Severity level 7 functions as severity level 6, save that it deals
        2d4 points of Constitution drain.</p>

        <p>Damage from fallout is a poison effect.</p>

        <p>Fallout is a type of radioactive weather, and thus requires the
        Radiation Lord advanced talent to create.</p>

        <h2 id="toc10"><span>Weather Talent Types</span></h2>

        <h4 id="toc11"><span>Mantle</span></h4>

        <p>When you gain your first (mantle) talent, you gain the ability, as a
        standard action, to touch a creature and spend a spell point, bestowing
        your mantle upon it. Unwilling targets may attempt a Will save to resist
        a mantle being placed on them. A mantle lasts for one hour per caster
        level, and is subject to spell resistance.</p>

        <p>Having a mantle grants different effects depending on the current
        weather conditions. Talents marked with the (mantle) tag add effects to
        your mantle; once you have bestowed your mantle upon a creature they
        gain the benefits of all (mantle) talents you have that apply to the
        weather in its area. Abilities and effects that reduces the severity of
        weather (such as the Weather incanter specialization or Clear Skies
        mantle) do not reduce the effect of mantles.</p>

        <h4 id="toc12"><span>Shroud</span></h4>

        <p>While control weather deals in manipulating large weather patterns, a
        shroud is an expression of extremely localized weather, usually only
        large enough to effect a single target.</p>

        <p>You may activate a (shroud) talent as a standard action, placing that
        shroud on a single creature within your control weather range. Placing a
        shroud on a target is subject to spell resistance, but a target does not
        get a saving throw to resist the application of a shroud. Shroud effects
        persist as long as the caster concentrates. The caster may always spend
        a spell point as a free action to allow the effect to continue for 1
        round per caster level without the need for concentration.</p>

        <p>Unlike control weather, shrouds produce no lasting effects; any rain
        evaporates immediately ice disappears when it leaves the target, etc.
        Shrouds do not counter the prevailing effects of the local weather (for
        example, Karakaze does not protect against the effects of Heat), but are
        also not affected by them (for example, Glare functions even in complete
        darkness and Heat Stroke works even in arctic levels of Cold).</p>

        <p>When a creature is affected by two or more shrouds of the same
        weather type (such as Aridity or Heat) or is affected by a shroud and is
        in an area of the shroud’s weather type of severity level 3 or above, it
        suffers an additional effect depending on the shroud’s type. If a shroud
        has more than one weather type, it can trigger the added effect of any
        of its types. Regardless of the number of shrouds affecting a target,
        any additional effect only affects them once per round. The saving throw
        DC and the caster level for any additional effect are equal to that of
        the shroud affecting them. If a target is affected by multiple shrouds
        with different DCs and caster levels, use the highest DC and caster
        level for the additional effect (even if the highest caster level and
        highest DC are from different shrouds).</p>

        <h5 id="toc13"><span>Shroud Additional Effects</span></h5>

        <p><strong>Aridity:</strong> The target takes a -4 penalty to its
        Constitution score, which may not reduce its Constitution score below 1.
        It may attempt a Fortitude save (at its unpenalized Fortitude save
        bonus) at the beginning of each turn to negate this penalty for one
        round.</p>

        <p><strong>Cold:</strong> The target takes a -2 penalty to attack rolls,
        concentration checks, and AC. It may attempt a Will save at the
        beginning of each turn to ignore this penalty for one round.</p>

        <p><strong>Fallout:</strong> The target takes 1 point of bleed damage
        for every 5 caster levels you possess.</p>

        <p><strong>Heat:</strong> The target takes a -4 penalty to its Strength
        score, which may not reduce its Strength score below 1. It may attempt a
        Fortitude save at the beginning of each turn to negate this penalty for
        one round.</p>

        <p><strong>Precipitation:</strong> Each round at the beginning of its
        turn the target must succeed at a Reflex save or treat the square it
        starts in as difficult terrain. They also take a -4 penalty to
        Acrobatics and Climb checks, and to CMB and CMD.</p>

        <p><strong>Wind:</strong> Each round at the beginning of its turn the
        target must succeed at a Fortitude save or be moved 1d6 x 5 feet in a
        random direction. This movement provokes attacks of opportunity. If the
        target cannot move this far (such as if a wall or other creature is in
        the way) it takes 1d6 damage per 10 feet not traveled (minimum 1d6) and
        falls prone.</p>

        <hr /><br />

        <br /><b>Weather Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.PoYJq9ur0ystoEDH]{Dry
        Lightning}</li>

        <li>@Compendium[pf1spheres.magic-talents.fSPbrP1VkCd6Djh8]{Encompassing
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.W2Xx7qI9utfqyFTW]{Focused
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.I32zYLyorP2IbRWL]{Forceful
        Wind}</li>

        <li>@Compendium[pf1spheres.magic-talents.3R8zDaLhtDE8VJCF]{Greater
        Size}</li>

        <li>@Compendium[pf1spheres.magic-talents.hWlEqedSeJq4sLiw]{Greater
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.0T8bJcSq9paSp5pW]{Head In The
        Clouds}</li>

        <li>@Compendium[pf1spheres.magic-talents.nxOJEC3ROAo2Zr7j]{Instill
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.vTfVP0MJLjzmTgSY]{Lengthened
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.S3hknX9OA30W6MOq]{Severe
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.aYaeUdgDH0uRZmAn]{Snow
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.h4r3ausH0g1n2CLW]{Storm
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.7fRw9AhvXJnPXuui]{Weather
        Strike}</li>

        </ul><br /><b>Mantle Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.pQQGyisLV9rwRMuG]{Barometric
        Mantle}</li>

        <li>@Compendium[pf1spheres.magic-talents.1qwQsS5J1Rx257TN]{Blazing
        Skin}</li>

        <li>@Compendium[pf1spheres.magic-talents.ctdmhSPOh8LQz6RH]{Boreal
        Glare}</li>

        <li>@Compendium[pf1spheres.magic-talents.7O9QWphgtCWYFUs4]{Borne
        Aloft}</li>

        <li>@Compendium[pf1spheres.magic-talents.lbp6vmEmiXgkzypD]{Clear
        Skies}</li>

        <li>@Compendium[pf1spheres.magic-talents.iS3VlcSPVT4IFWKU]{Desiccant}</li>

        <li>@Compendium[pf1spheres.magic-talents.fWilEOvEzRxtvsU4]{Fluidity}</li>

        <li>@Compendium[pf1spheres.magic-talents.jkgna9gfol4Soh5R]{Frost
        Shield}</li>

        <li>@Compendium[pf1spheres.magic-talents.P62yJLvdwbBRLudH]{Gelid
        Body}</li>

        <li>@Compendium[pf1spheres.magic-talents.LBOvGt5s7OVTl2hX]{High
        Energy}</li>

        <li>@Compendium[pf1spheres.magic-talents.gdOPE8thFlqdzsUp]{Lightning
        Rod}</li>

        <li>@Compendium[pf1spheres.magic-talents.9FndsT7jaHAveqtM]{Mirage
        Sight}</li>

        <li>@Compendium[pf1spheres.magic-talents.tfH2sQmNqrjxSGyH]{Mist
        Form}</li>

        <li>@Compendium[pf1spheres.magic-talents.MLBKAHflnmlxJcza]{Mummified
        Flesh}</li>

        <li>@Compendium[pf1spheres.magic-talents.uXMbDmpt8GiSDZ72]{Razor
        Ice}</li>

        <li>@Compendium[pf1spheres.magic-talents.imwgQya0ZODkl3kJ]{Sand
        Swimmer}</li>

        <li>@Compendium[pf1spheres.magic-talents.cw9RvBXWSu5VCmRr]{Sodden}</li>

        <li>@Compendium[pf1spheres.magic-talents.qZQkKm1jngZwUwDp]{This Sparks
        Joy}</li>

        <li>@Compendium[pf1spheres.magic-talents.VJ0Sg9RekYchbQFY]{Whispering
        Winds}</li>

        <li>@Compendium[pf1spheres.magic-talents.r52vPXJBVfCmyohd]{Wind
        Tunnel}</li>

        <li>@Compendium[pf1spheres.magic-talents.154iTYp3Mghht2If]{Zephyr’s
        Flight}</li>

        </ul><br /><b>Shroud Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.aDSZXlZOAA0iaGb6]{Battering
        Winds}</li>

        <li>@Compendium[pf1spheres.magic-talents.SNafNYCokWeSkSt4]{Biting
        Wind}</li>

        <li>@Compendium[pf1spheres.magic-talents.GaRQsD8dMqS0Gc4Q]{Black
        Ice}</li>

        <li>@Compendium[pf1spheres.magic-talents.vKYi7MgZBP2ov2Gj]{Crackling
        Arc}</li>

        <li>@Compendium[pf1spheres.magic-talents.5De1PfZ8DzCCYffG]{Diamond
        Dust}</li>

        <li>@Compendium[pf1spheres.magic-talents.bHUJ0YuLwF9JocpU]{Drench}</li>

        <li>@Compendium[pf1spheres.magic-talents.x1jNCSV5jSh4I3US]{Dust
        Cloud}</li>

        <li>@Compendium[pf1spheres.magic-talents.eZ7lfuUyg3QPiQXA]{Fata
        Morgana}</li>

        <li>@Compendium[pf1spheres.magic-talents.GTTptjC9T6AWzAkN]{Glare}</li>

        <li>@Compendium[pf1spheres.magic-talents.zlC947loJo5cwP2r]{Heat
        Stroke}</li>

        <li>@Compendium[pf1spheres.magic-talents.SHx41lOvdDySfZz6]{Illuminating
        Shaft}</li>

        <li>@Compendium[pf1spheres.magic-talents.2KTHnQaaxJXE9o6D]{Intensified
        Weather}</li>

        <li>@Compendium[pf1spheres.magic-talents.JL6kmsBeaWh1Gd6t]{Karakaze}</li>

        <li>@Compendium[pf1spheres.magic-talents.KsP9AwKohcWcdW4i]{Personal
        Thunderhead}</li>

        <li>@Compendium[pf1spheres.magic-talents.T5LLDeXB0SVOfA6v]{Sirocco}</li>

        <li>@Compendium[pf1spheres.magic-talents.MdYcqnFAB44F7ji0]{Squamish}</li>

        <li>@Compendium[pf1spheres.magic-talents.CcWN9ecJ4WqotdXJ]{This Does Not
        Spark Joy}</li>

        </ul><br /><b>Advanced Weather Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.JJC3bpM5rteqkh60]{Biohazard}</li>

        <li>@Compendium[pf1spheres.magic-talents.DUiIyLiMULEICtlc]{Boiling
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.6waHR0ri4UMVKNQW]{Cacaphonic
        Clap}</li>

        <li>@Compendium[pf1spheres.magic-talents.8dMbLoPu51cr2okb]{Climate}</li>

        <li>@Compendium[pf1spheres.magic-talents.eiA6ZFkDH7WBiiq5]{Climatic
        Shift}</li>

        <li>@Compendium[pf1spheres.magic-talents.GuxjsbYxsEI7hXTr]{Cold
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.SvS8ShY007u2078p]{Contaminated
        Blows}</li>

        <li>@Compendium[pf1spheres.magic-talents.PMB9PoyBUDRqDwmY]{Contamination
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.8Wj1sdjYQZqMj9p2]{Dehydration
        Pulse}</li>

        <li>@Compendium[pf1spheres.magic-talents.xtmvAEF1SWnPRylo]{Desert
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.oEmR2lqIGJbjHUFa]{Fire
        Tornadoes}</li>

        <li>@Compendium[pf1spheres.magic-talents.zKYa1p2K2lQZ1zSZ]{Heat
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.nhGglrmqbTBkUN3S]{Interference
        Cloud}</li>

        <li>@Compendium[pf1spheres.magic-talents.ApL2b0gRgCyOepsf]{Melt
        Skin}</li>

        <li>@Compendium[pf1spheres.magic-talents.8vXpn6lHbjvzAXuC]{Radiation
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.zODMwg08OtUnEMz9]{Radiotherapy}</li>

        <li>@Compendium[pf1spheres.magic-talents.zW2i7P4ptVDCDwmx]{Rain
        Bomb}</li>

        <li>@Compendium[pf1spheres.magic-talents.1Z75TyOV2YLeJA6Y]{Rain
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.m4GTZvJ0zkiLApug]{Tornado
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.NY0b1cRdUYIrJ4EH]{Volcano
        Lord}</li>

        <li>@Compendium[pf1spheres.magic-talents.d9d0VCbZhLGGuEwn]{Wind
        Lord}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
